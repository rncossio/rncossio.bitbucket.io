$( document ).ready(function() {
    //abrir cerrar menu mobile
    $( ".toggle-mobile-menu" ).click(function() {
        $('#navMobile').attr('style','width: 290px');
        $('.over-content').show();
    });
    $( ".closebtn, .over-content" ).click(function() {
        $('#navMobile').attr('style','width: 0px');
        $('.over-content').hide();
    });

    $('.tooltips').tooltip();

    $('[data-toggle="popover"]').popover();   

    $(window).scroll(function () {

        headerHeight = $('#header-nav').height();
        var reachTop = $('.get-top').offset().top - headerHeight;
        var alturaMenu = $('.stick-top').height();
        if ($(this).scrollTop() > reachTop) {
            $('.stick-top').addClass('stick-menu');
            $('.get-top').css("height", alturaMenu);
        }
        else {
            $('.stick-top').removeClass('stick-menu');
            $('.get-top').css("height", "0px");
        }

    });

});

